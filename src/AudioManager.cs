﻿using FMOD;
using FMOD.Studio;
using System;
using System.Collections.Generic;

namespace SixteenBitNuts.FMODStudio
{
    /// <summary>
    /// Implementation of a 16-bit Nuts AudioManager for FMOD Studio
    /// </summary>
    public class AudioManager : IAudioManager
    {
        public FMOD.Studio.System AudioSystem => audioSystem;

        private FMOD.Studio.System audioSystem;
        private readonly Dictionary<string, Bank> banks;
        private EventInstance currentMusic;

        /// <summary>
        /// Constructor of the FMOD Studio AudioManager
        /// </summary>
        public AudioManager()
        {
            banks = new Dictionary<string, Bank>();

            // System creation
            RESULT result = FMOD.Studio.System.create(out audioSystem);
            if (result != RESULT.OK)
            {
                System.Diagnostics.Debug.WriteLine(Error.String(result));
            }
        }

        /// <summary>
        /// Audio system initialization
        /// </summary>
        public void Initialize()
        {
            RESULT result = audioSystem.initialize(32, FMOD.Studio.INITFLAGS.NORMAL, FMOD.INITFLAGS.NORMAL, IntPtr.Zero);
            if (result != RESULT.OK)
            {
                System.Diagnostics.Debug.WriteLine(Error.String(result));
            }
        }

        /// <summary>
        /// Load content without parameters is not available for this implementation
        /// </summary>
        public virtual void LoadContent()
        {
            throw new NotImplementedException("LoadContent without parameters is not implemented in FMOD Studio AudioManager. Use LoadContent(string[] bankNames) instead.");
        }

        /// <summary>
        /// Load specified banks
        /// </summary>
        /// <param name="bankNames">Bank names</param>
        public virtual void LoadContent(string[] bankNames)
        {
            foreach (var bankName in bankNames)
            {
                RESULT result = audioSystem.loadBankFile("Content/Audio/Desktop/" + bankName + ".bank", LOAD_BANK_FLAGS.NORMAL, out Bank bank);
                if (result != RESULT.OK)
                {
                    System.Diagnostics.Debug.WriteLine(Error.String(result));
                }
                banks[bankName] = bank;
            }
        }

        /// <summary>
        /// Update the FMOD Studio audio system
        /// </summary>
        public void Update()
        {
            audioSystem.update();
        }

        /// <summary>
        /// Play a music event by name
        /// </summary>
        /// <param name="name">Name of the music event</param>
        public void PlayMusic(string name)
        {
            RESULT result = audioSystem.getEvent("event:/Music/" + name, out EventDescription eventDescription);
            if (result != RESULT.OK)
            {
                System.Diagnostics.Debug.WriteLine(Error.String(result) + " (event:/Music/" + name + ")");
            }
            eventDescription.createInstance(out currentMusic);

            result = eventDescription.loadSampleData();
            if (result == RESULT.OK)
            {
                currentMusic.start();
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(Error.String(result));
            }
        }

        /// <summary>
        /// Set a new value for a given music parameter
        /// </summary>
        /// <param name="name">Parameter name</param>
        /// <param name="value">New parameter value</param>
        public void SetMusicParameter(string name, float value)
        {
            currentMusic.setParameterByName(name, value);
        }

        /// <summary>
        /// Play a sound event by name
        /// </summary>
        /// <param name="name">Name of the sound event</param>
        public void PlaySound(string name)
        {
            RESULT result = audioSystem.getEvent("event:/SFX/" + name, out EventDescription eventDescription);
            if (result != RESULT.OK)
            {
                System.Diagnostics.Debug.WriteLine(Error.String(result) + " (event:/SFX/" + name + ")");
            }

            result = eventDescription.createInstance(out EventInstance sound);
            if (result == RESULT.OK)
            {
                sound.start();
            }
        }

        /// <summary>
        /// Unload all FMOD Studio banks
        /// </summary>
        public void UnloadContent()
        {
            foreach (var bank in banks)
            {
                bank.Value.unload();
            }
        }

        /// <summary>
        /// Free resources
        /// </summary>
        public void Dispose()
        {
            audioSystem.release();
        }
    }
}
